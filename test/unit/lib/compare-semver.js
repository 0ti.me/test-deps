const { d, expect, tquire } = deps;

const me = __filename;

const compareSemver = tquire(me);

d(me, () => {
  it('should indicate throw an error if a is unable to be parsed', () =>
    expect(() => compareSemver('hello', '1.0.0')).to.throw(
      /The first semver.*/,
    ));

  it('should indicate throw an error if b is unable to be parsed', () =>
    expect(() => compareSemver('1.0.0', 'hello')).to.throw(
      /The second semver.*/,
    ));

  it('should indicate that 1.0.0 and 1.0.0-alpha are equal', () =>
    expect(compareSemver('1.0.0', '1.0.0-alpha')).to.equal(0));

  it('should indicate that 1.0.0-alpha and 1.0.0 are equal', () =>
    expect(compareSemver('1.0.0-alpha', '1.0.0')).to.equal(0));

  it('should indicate that 0.0.1 and 0.0.1 are equal', () =>
    expect(compareSemver('0.0.1', '0.0.1')).to.equal(0));

  it('should indicate that 0.0.1 is greater than 0.0.0', () =>
    expect(compareSemver('0.0.1', '0.0.0')).to.equal(1));

  it('should indicate that 0.0.0 is less than 0.0.1', () =>
    expect(compareSemver('0.0.0', '0.0.1')).to.equal(-1));

  it('should indicate 1.0.0 is less than 1.0.0-alpha1', () =>
    expect(compareSemver('1.0.0', '1.0.0-alpha1')).to.equal(-1));

  it('should indicate 1.0.0-alpha1 is greater than 1.0.0', () =>
    expect(compareSemver('1.0.0-alpha1', '1.0.0')).to.equal(1));

  it('should indicate 1.0.0-alpha2 is greater than 1.0.0-alpha1', () =>
    expect(compareSemver('1.0.0-alpha2', '1.0.0-alpha1')).to.equal(1));
});
