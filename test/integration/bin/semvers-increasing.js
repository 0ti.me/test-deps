const semversIncreasing = require('../../../bin/semvers-increasing');

const { d, expect } = deps;

const me = __filename;

d(me, () => {
  it('should return 0 if left is less than right', () =>
    expect(semversIncreasing('0.0.0', '0.0.1')).to.equal(0));

  it('should return 1 if left is equal to right', () =>
    expect(semversIncreasing('0.0.1', '0.0.1')).to.equal(1));

  it('should return 2 if left is greater than right', () =>
    expect(semversIncreasing('0.0.1', '0.0.0')).to.equal(2));
});
