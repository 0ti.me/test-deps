const path = require('path');

// A shortcut for mocha describe that just describes the file path
module.exports =
  (config) =>
  (filename, ...args) => {
    const cb = args.pop();

    return config.describe(
      [path.relative(config.utilities.info.directories.test, filename)]
        .concat(args)
        .join(' '),
      cb,
    );
  };
